package com.openretail.openretailpos.model;

public class WidgetListViewModel {
    private String Title;
    public String getTitle() { return this.Title; }
    public void setTitle(String title) { this.Title = title; }

    private String Description;
    public String getDescription() { return this.Description; }
    public void setDescription(String description) { this.Description = description; }

    private String ImageID;
    public String getImageID() { return this.ImageID; }
    public void setImageID(String imageID) { this.ImageID = imageID; }
}
