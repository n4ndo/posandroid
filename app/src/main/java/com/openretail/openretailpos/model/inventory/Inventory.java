package com.openretail.openretailpos.model.inventory;

import com.openretail.openretailpos.services.NoDaoException;
import com.openretail.openretailpos.services.inventory.IInventoryDao;
import com.openretail.openretailpos.services.inventory.InventoryDao;

/**
 * This class is service locater for Product Catalog and Stock.
 * 
 * @author Refresh Team
 * 
 */
public class Inventory {

	private Stock stock;
	private ProductCatalog productCatalog;
	private static Inventory instance = null;
	private static IInventoryDao inventoryDao = null;

	/**
	 * Constructs Data Access Object of inventory. 
	 * @throws NoDaoException if DAO is not exist.
	 */
	private Inventory() throws NoDaoException {
		if (!isDaoSet()) {
			throw new NoDaoException();
		}
		stock = new Stock(inventoryDao);
		productCatalog = new ProductCatalog(inventoryDao);
	}

	/**
	 * Determines whether the DAO already set or not.
	 * @return true if the DAO already set; otherwise false.
	 */
	public static boolean isDaoSet() {
		return inventoryDao != null;
	}

	/**
	 * Sets the database connector.
	 * @param dao Data Access Object of inventory.
	 */
	public static void setInventoryDao(IInventoryDao dao) {
		inventoryDao = dao;
	}

	/**
	 * Returns product catalog using in this inventory.
	 * @return product catalog using in this inventory.
	 */
	public ProductCatalog getProductCatalog() {
		return productCatalog;
	}

	/**
	 * Returns stock using in this inventory.
	 * @return stock using in this inventory.
	 */
	public Stock getStock() {
		return stock;
	}

	/**
	 * Returns the instance of this singleton class.
	 * @return instance of this class.
	 * @throws NoDaoException if DAO was not set.
	 */
	public static Inventory getInstance() throws NoDaoException {
		if (instance == null)
			instance = new Inventory();
		return instance;
	}

}
