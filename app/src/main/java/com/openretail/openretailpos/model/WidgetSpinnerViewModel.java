package com.openretail.openretailpos.model;

public class WidgetSpinnerViewModel {
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    private String id;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    private String title;

    public WidgetSpinnerViewModel(String id, String title){
        this.id = id;
        this.title = title;
    }

    @Override
    public String toString() {
        return title;
    }
}
