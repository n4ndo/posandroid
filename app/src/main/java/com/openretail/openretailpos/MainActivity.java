package com.openretail.openretailpos;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.SearchView;
import android.view.MenuInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.openretail.openretailpos.base.ActionBarDrawerToggleExtended;
import com.openretail.openretailpos.base.BaseActivity;
import com.openretail.openretailpos.base.SpinnerBinder;
import com.openretail.openretailpos.fragments.ProductFragment;
import com.openretail.openretailpos.fragments.ReceiptFragment;
import com.openretail.openretailpos.fragments.ReportFragment;
import com.openretail.openretailpos.fragments.SalesFragment;
import com.openretail.openretailpos.model.WidgetSpinnerViewModel;

import java.util.ArrayList;
import java.util.Locale;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener, SearchView.OnQueryTextListener {

    private DrawerLayout drawer;
    private ActionBarDrawerToggleExtended toggle;
    private NavigationView navigationView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupNavigationItems();
        setupDrawerAndToggle();
        setupDummySpinnerRecord();

        if(savedInstanceState == null) {
            showSales();
            navigationView.setCheckedItem(R.id.nav_sales);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main, menu);

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            //return true;
        }
        else if (id == R.id.lang_en){
            setLanguage("en");
        }
        else if (id == R.id.lang_id){
            setLanguage("id");
        }

        return super.onOptionsItemSelected(item);
    }
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        switch (id)
        {
            case R.id.nav_sales :
                showSales();
                break;
            case R.id.nav_receipt :
                showReceipt();
                break;
            case R.id.nav_product :
                showProduct();
                break;
            case R.id.nav_report :
                showReport();
                break;
            case R.id.nav_sync:
                Toast.makeText(this,"Synced",Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_share:
                Toast.makeText(this,"Shared",Toast.LENGTH_SHORT).show();
                break;
            case R.id.nav_send:
                Toast.makeText(this,"Sended",Toast.LENGTH_SHORT).show();
                break;
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showSales(){
        add(SalesFragment.newInstance(R.string.nav_sales, R.layout.fragment_sales));
    }

    private void showReceipt(){
        add(ReceiptFragment.newInstance(R.string.nav_receipt, R.layout.fragment_receipt));
    }

    private void showProduct(){
        add(ProductFragment.newInstance(R.string.nav_product, R.layout.fragment_product));
    }

    private void showReport(){
        add(ReportFragment.newInstance(R.string.nav_report, R.layout.fragment_report));
    }

    private void setLanguage(String localeString) {
        Locale locale = new Locale(localeString);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config,
                getBaseContext().getResources().getDisplayMetrics());
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

    private void setupNavigationItems() {
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        final FloatingActionButton floatingSales = (FloatingActionButton) findViewById(R.id.floating_sales);
        floatingSales.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ForceResetBackStack();
                showSales();
                navigationView.setCheckedItem(R.id.nav_sales);
            }
        });

        final FloatingActionButton floatingAdd = (FloatingActionButton) findViewById(R.id.floating_add);
        floatingAdd.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getBaseContext(),"Add Record",Toast.LENGTH_SHORT).show();
                addCurrentFragmentDetail();
            }
        });
    }

    private void setupDrawerAndToggle() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_main);
        Toolbar toolbar2 = (Toolbar) findViewById(R.id.toolbar_search);
        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        toolbar.inflateMenu(R.menu.main);
        //toolbar2.inflateMenu(R.menu.action);//changed
        //AppCompatButton button = findViewById(R.id.menu_toolbarsave);
        //button.setBackgroundResource(R.color.colorPrimary);
        //button.setText("Save");

//        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
//        MenuItem mSearchmenuItem = menu.findItem(R.id.menu_toolbarsearch);
//        SearchView searchView = (SearchView) mSearchmenuItem.getActionView();
//        searchView.setQueryHint("enter Text");
//        searchView.setOnQueryTextListener(this);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        toggle = new ActionBarDrawerToggleExtended(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
    }

    private void setupDummySpinnerRecord(){
        Toolbar toolbar2 = (Toolbar) findViewById(R.id.toolbar_search);
        AppCompatSpinner spinner = findViewById(R.id.menu_toolbarspinner);
        ArrayList<WidgetSpinnerViewModel> records = new ArrayList<WidgetSpinnerViewModel>();
        records.add(new WidgetSpinnerViewModel("1","All Item"));
        records.add(new WidgetSpinnerViewModel("2","Bebek"));
        records.add(new WidgetSpinnerViewModel("3","Ayam"));
        records.add(new WidgetSpinnerViewModel("4","Mie"));

        SpinnerBinder.Bind(getApplicationContext(),spinner,records);
    }

    protected DrawerLayout getDrawer() {
        return drawer;
    }

    protected ActionBarDrawerToggle getDrawerToggle() {
        return toggle;
    }
}
