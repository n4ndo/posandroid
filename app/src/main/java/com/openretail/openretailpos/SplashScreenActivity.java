package com.openretail.openretailpos;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.openretail.openretailpos.model.DateTimeStrategy;
import com.openretail.openretailpos.model.LanguageController;
import com.openretail.openretailpos.model.inventory.Inventory;
import com.openretail.openretailpos.model.sale.Register;
import com.openretail.openretailpos.model.sale.SaleLedger;
import com.openretail.openretailpos.services.DatabaseExecutor;
import com.openretail.openretailpos.services.IDatabase;
import com.openretail.openretailpos.services.OpenRetailDatabase;
import com.openretail.openretailpos.services.inventory.IInventoryDao;
import com.openretail.openretailpos.services.inventory.InventoryDao;
import com.openretail.openretailpos.services.sales.ISaleDao;
import com.openretail.openretailpos.services.sales.SaleDao;

import gr.net.maroulis.library.EasySplashScreen;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        IDatabase database = new OpenRetailDatabase(this);
        IInventoryDao inventoryDao = new InventoryDao(database);
        ISaleDao saleDao = new SaleDao(database);
        DatabaseExecutor.setDatabase(database);
        LanguageController.setDatabase(database);

        Inventory.setInventoryDao(inventoryDao);
        Register.setSaleDao(saleDao);
        SaleLedger.setSaleDao(saleDao);

        DateTimeStrategy.setLocale("en", "EN");

        EasySplashScreen config = new EasySplashScreen(SplashScreenActivity.this)
                .withFullScreen()
                .withTargetActivity(MainActivity.class)
                .withSplashTimeOut(2000)
                .withBackgroundColor(Color.parseColor("#FFFFFFFF"))
                .withHeaderText("")
                .withFooterText("")
                .withBeforeLogoText("")
                .withBeforeLogoText("")
                .withAfterLogoText("")
                .withLogo(R.mipmap.ic_launcher_splash);

        config.getHeaderTextView().setTextColor(Color.WHITE);
        config.getFooterTextView().setTextColor(Color.WHITE);
        config.getBeforeLogoTextView().setTextColor(Color.WHITE);
        config.getAfterLogoTextView().setTextColor(Color.WHITE);

        View easySplashScreen = config.create();
        setContentView(easySplashScreen);
    }
}
