package com.openretail.openretailpos.base;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.openretail.openretailpos.R;

public abstract class BaseFragment extends Fragment implements IBaseFragment {

    private int titleResourceKey;
    private int viewResourceKey;
    private String title = "";
    private Boolean isFragmentAddEdit = false;
    private Boolean isActionAddActive = false;
    private Boolean isActionGroupSearchActive = false;
    private Boolean isActionTextSearchActive = false;

    public void setFragmentAddEdit(Boolean fragmentAddEdit) {
        isFragmentAddEdit = fragmentAddEdit;
    }

    private AddFragmentHandler fragmentHandler;

    public void setResourceKey(int titleResourceKey, int viewResourceKey)
    {
        this.titleResourceKey = titleResourceKey;
        this.viewResourceKey = viewResourceKey;
    }

    public void setResourceKey(String titleText, int viewResourceKey)
    {
        this.title = titleText;
        this.viewResourceKey = viewResourceKey;
    }

    public void setSearchBarVisibility(Boolean actionGroupSearch, Boolean actionTextSearch, Boolean isActionAddActive){
        this.isActionGroupSearchActive = actionGroupSearch;
        this.isActionTextSearchActive = actionTextSearch;
        this.isActionAddActive = isActionAddActive;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        fragmentHandler = new AddFragmentHandler(getActivity().getSupportFragmentManager());
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        this.title = getText(titleResourceKey).toString();
        View view = inflater.inflate(viewResourceKey, container, false);
        onCreateViewAction(inflater,view,container,savedInstanceState);

        Toolbar toolbar = getActivity().findViewById(R.id.toolbar_search);
        FloatingActionButton floatButton = getActivity().findViewById(R.id.floating_add);

        AppCompatSpinner spinner = getActivity().findViewById(R.id.menu_toolbarspinner);
        SearchView textSearch = getActivity().findViewById(R.id.menu_toolbarsearch);
        AppCompatButton button = getActivity().findViewById(R.id.menu_toolbarsave);

        if(spinner != null) {
            if (isActionGroupSearchActive) {
                spinner.setVisibility(View.VISIBLE);
            } else {
                spinner.setVisibility(View.GONE);
            }
        }

        if(textSearch != null) {
            if (isActionTextSearchActive) {
                textSearch.setVisibility(View.VISIBLE);
            } else {
                textSearch.setVisibility(View.GONE);
            }
        }

        if(button != null) {
            if (isActionAddActive) {
                button.setVisibility(View.VISIBLE);
            } else {
                button.setVisibility(View.GONE);
            }
        }

        if(toolbar != null){
            if(isActionTextSearchActive || isActionGroupSearchActive || isActionAddActive) {
                toolbar.setVisibility(View.VISIBLE);
            }
            else{
                toolbar.setVisibility(View.GONE);
            }
        }

        if(floatButton != null){
            if(!isFragmentAddEdit){
                floatButton.setVisibility(View.GONE);
            }else{
                floatButton.setVisibility(View.VISIBLE);
            }
        }

        return view;
    }

    protected boolean onCreateViewAction(@NonNull LayoutInflater inflater, @NonNull View view, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState){
        return true;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getTitle());
    }

    public String getTitle() { return title; };

    public abstract Boolean getIsMainFragment();

    protected void add(IBaseFragment fragment) {
        fragmentHandler.add(fragment);
    }

    public void setFragmentAddEdit(boolean fragmentAddEdit) {
        isFragmentAddEdit = fragmentAddEdit;
    }

    public abstract IBaseFragment getFragmentDetail();
}
