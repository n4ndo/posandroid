package com.openretail.openretailpos.base;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.openretail.openretailpos.R;
import com.openretail.openretailpos.model.WidgetSpinnerViewModel;

import java.util.ArrayList;
import java.util.List;

public class SpinnerBinder {
    public static void Bind(Context context, Spinner spinner, List<WidgetSpinnerViewModel> records)
    {
        ArrayList<WidgetSpinnerViewModel> tmpRecords = new ArrayList<WidgetSpinnerViewModel>();
        for(int i = 0; i < records.size(); i++) {
            tmpRecords.add(records.get(i));
        }

        //ArrayAdapter<WidgetSpinnerViewModel> adapter = new ArrayAdapter<WidgetSpinnerViewModel>(context,android.R.layout.simple_spinner_item, records);
        ArrayAdapter<WidgetSpinnerViewModel> adapter = new ArrayAdapter<WidgetSpinnerViewModel>(context, R.layout.spinner_item, records);
        adapter.setDropDownViewResource(R.layout.spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }
}
