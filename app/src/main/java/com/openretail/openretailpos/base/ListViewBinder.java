package com.openretail.openretailpos.base;

import android.content.Context;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.openretail.openretailpos.R;
import com.openretail.openretailpos.model.WidgetListViewModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ListViewBinder {
    public static void Bind(Context context, ListView listView, List<WidgetListViewModel> records)
    {
        List<HashMap<String,String>> tmpRecords = new ArrayList<HashMap<String, String>>();
        for(int i = 0; i < records.size(); i++) {
            HashMap<String,String> hm = new HashMap<String, String>();
            hm.put("widget_listview_title",records.get(i).getTitle());
            hm.put("widget_listview_description", records.get(i).getDescription());
            hm.put("widget_listview_image", records.get(i).getImageID());
            tmpRecords.add(hm);
        }

        String[] from = {"widget_listview_title", "widget_listview_description", "widget_listview_image"};
        int[] to = {R.id.widget_listview_title, R.id.widget_listview_description, R.id.widget_listview_image};

        SimpleAdapter adapter = new SimpleAdapter(context,tmpRecords,R.layout.widget_listview,from,to);
        listView.setAdapter(adapter);
    }

    public static void Bind(Context context, ListView listView, ArrayList<String> records)
    {
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(context,
                android.R.layout.simple_list_item_1, android.R.id.text1, records);
        listView.setAdapter(adapter);
    }
}
