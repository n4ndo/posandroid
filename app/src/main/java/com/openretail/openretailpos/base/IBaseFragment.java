package com.openretail.openretailpos.base;

public interface IBaseFragment {
    String getTitle();
    Boolean getIsMainFragment();
    IBaseFragment getFragmentDetail();
}
