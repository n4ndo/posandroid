package com.openretail.openretailpos.base;

import android.app.Activity;
import android.graphics.drawable.Drawable;
import android.support.annotation.StringRes;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.widget.Toolbar;
import android.view.View;

public class ActionBarDrawerToggleExtended extends android.support.v7.app.ActionBarDrawerToggle {

    public ActionBarDrawerToggleExtended(Activity activity, DrawerLayout drawerLayout,
                                 Toolbar toolbar, @StringRes int openDrawerContentDescRes,
                                 @StringRes int closeDrawerContentDescRes) {
        super(activity,drawerLayout,toolbar,openDrawerContentDescRes,closeDrawerContentDescRes);
    }

    @Override
    public void onDrawerOpened(View drawerView) {
        super.onDrawerOpened(drawerView);
        setDrawerIndicatorEnabled(true);
    }

    @Override
    public void onDrawerClosed(View drawerView) {
        super.onDrawerClosed(drawerView);
    }
}
