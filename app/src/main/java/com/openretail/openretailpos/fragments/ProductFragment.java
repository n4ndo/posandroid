package com.openretail.openretailpos.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.openretail.openretailpos.R;
import com.openretail.openretailpos.base.BaseFragment;
import com.openretail.openretailpos.base.IBaseFragment;

public class ProductFragment extends BaseFragment {

    public static IBaseFragment newInstance(int titleResourceKey, int viewResourceKey) {
        ProductFragment fragment = new ProductFragment();
        fragment.setResourceKey(titleResourceKey,viewResourceKey);
        fragment.setSearchBarVisibility(false,false, false);
        fragment.setFragmentAddEdit(false);
        return (IBaseFragment)fragment;
    }

    @Override
    protected boolean onCreateViewAction(@NonNull LayoutInflater inflater, @NonNull View view, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        NavigationView navigation = view.findViewById(R.id.nav_product);
        navigation.setNavigationItemSelectedListener(navListener);
        return super.onCreateViewAction(inflater, view, container, savedInstanceState);
    }

    @Override
    public Boolean getIsMainFragment() {
        return true;
    }

    @Override
    public IBaseFragment getFragmentDetail() {
        return null;
    }

    private NavigationView.OnNavigationItemSelectedListener navListener =
            new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    int id = item.getItemId();
                    switch (id)
                    {
                        case R.id.nav_product_items:
                            add(ProductItemsFragment.newInstance(R.string.nav_product_items_title, R.layout.fragment_product_items));
                            break;
                        case R.id.nav_product_categories:
                            add(ProductCategoriesFragment.newInstance(R.string.nav_product_categories_title, R.layout.fragment_product_categories));
                            break;
                        case R.id.nav_product_discounts:
                            add(ProductDiscountsFragment.newInstance(R.string.nav_product_discounts_title, R.layout.fragment_product_discounts));
                            break;
                    }
                    return true;
                }
            };
}
