package com.openretail.openretailpos.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.openretail.openretailpos.R;
import com.openretail.openretailpos.base.BaseFragment;
import com.openretail.openretailpos.base.IBaseFragment;
import com.openretail.openretailpos.base.ListViewBinder;
import com.openretail.openretailpos.model.WidgetListViewModel;

import java.util.ArrayList;
import java.util.List;

public class ProductCategoriesFragment extends BaseFragment {

    public static IBaseFragment newInstance(int titleResourceKey, int viewResourceKey) {
        ProductCategoriesFragment fragment = new ProductCategoriesFragment();
        fragment.setResourceKey(titleResourceKey,viewResourceKey);
        fragment.setSearchBarVisibility(true,true, false);
        fragment.setFragmentAddEdit(true);
        return (IBaseFragment)fragment;
    }

    protected boolean onCreateViewAction(@NonNull LayoutInflater inflater, @NonNull View view, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        ListView listView = (ListView)view.findViewById(R.id.product_categories_list);

        String[] values = new String[] { "Bebek", "Ayam", "Mie Sornop"};
        List<WidgetListViewModel> records = new ArrayList<>();

        for(int i = 0; i < values.length; i++) {
            WidgetListViewModel item = new WidgetListViewModel();
            item.setTitle(values[i]);
            item.setDescription("");
            item.setImageID(Integer.toString(R.drawable.ic_menu_product_discounts));
            records.add(item);
        }

        listView.setOnItemClickListener(
                new AdapterView.OnItemClickListener() {
                    public void onItemClick(AdapterView<?> adapter, View v, int position, long id) {
                        add(ProductDiscountsEditFragment.newInstance(R.string.nav_product_categories_category_title,R.layout.fragment_product_category_edit));
                    }
                }
        );

        ListViewBinder.Bind(getContext(),listView,records);

        return super.onCreateViewAction(inflater, view, container, savedInstanceState);
    }

    @Override
    public Boolean getIsMainFragment() {
        return false;
    }

    @Override
    public IBaseFragment getFragmentDetail() {
        return ProductDiscountsEditFragment.newInstance(R.string.nav_product_categories_category_title,R.layout.fragment_product_category_edit);
    }
}
