package com.openretail.openretailpos.fragments;

import com.openretail.openretailpos.base.BaseFragment;
import com.openretail.openretailpos.base.IBaseFragment;

public class ProductCategoriesEditFragment extends BaseFragment {

    public static IBaseFragment newInstance(int titleResourceKey, int viewResourceKey) {
        ProductCategoriesEditFragment fragment = new ProductCategoriesEditFragment();
        fragment.setResourceKey(titleResourceKey,viewResourceKey);
        fragment.setSearchBarVisibility(false,false, true);
        fragment.setFragmentAddEdit(false);
        return (IBaseFragment)fragment;
    }

    @Override
    public Boolean getIsMainFragment() {
        return false;
    }

    @Override
    public IBaseFragment getFragmentDetail() {
        return null;
    }
}
