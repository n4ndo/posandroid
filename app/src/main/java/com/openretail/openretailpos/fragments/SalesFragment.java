package com.openretail.openretailpos.fragments;

import com.openretail.openretailpos.base.BaseFragment;
import com.openretail.openretailpos.base.IBaseFragment;

public class SalesFragment extends BaseFragment {

    public static IBaseFragment newInstance(int titleResourceKey, int viewResourceKey) {
        SalesFragment fragment = new SalesFragment();
        fragment.setResourceKey(titleResourceKey,viewResourceKey);
        fragment.setSearchBarVisibility(false,false, false);
        fragment.setFragmentAddEdit(false);
        return (IBaseFragment)fragment;
    }

    @Override
    public Boolean getIsMainFragment() {
        return true;
    }

    @Override
    public IBaseFragment getFragmentDetail() {
        return null;
    }
}
