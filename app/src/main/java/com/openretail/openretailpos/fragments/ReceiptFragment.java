package com.openretail.openretailpos.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.openretail.openretailpos.R;
import com.openretail.openretailpos.base.BaseFragment;
import com.openretail.openretailpos.base.IBaseFragment;
import com.openretail.openretailpos.base.ListViewBinder;
import com.openretail.openretailpos.model.WidgetListViewModel;

import java.util.ArrayList;
import java.util.List;

public class ReceiptFragment extends BaseFragment {
    public static IBaseFragment newInstance(int titleResourceKey, int viewResourceKey) {
        ReceiptFragment fragment = new ReceiptFragment();
        fragment.setResourceKey(titleResourceKey,viewResourceKey);
        fragment.setSearchBarVisibility(false,true, false);
        fragment.setFragmentAddEdit(false);
        return (IBaseFragment)fragment;
    }

    @Override
    protected boolean onCreateViewAction(@NonNull LayoutInflater inflater, @NonNull View view, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ListView listView = (ListView)view.findViewById(R.id.receipt_list);

        String[] values = new String[] { "Android", "IOS", "Windows", "Linux", "aweawef", "awefawef",
                "awefawefawef","fgwefwef","awefawefawef","gwefwefwef","awefawefawe","gfwewefwef",
                "hhhrh","jrhjyjy","qwewewe","qweqwewe","zdvsdvs","ntrhrth","qwewefwefe","nntyny"
        };
        List<WidgetListViewModel> records = new ArrayList<>();

        for(int i = 0; i < values.length; i++) {
            WidgetListViewModel item = new WidgetListViewModel();
            item.setTitle(values[i]);
            item.setDescription("");
            item.setImageID(Integer.toString(R.drawable.ic_menu_receipt));
            records.add(item);
        }

        ListViewBinder.Bind(getContext(),listView,records);

        return super.onCreateViewAction(inflater, view, container, savedInstanceState);
    }

    @Override
    public Boolean getIsMainFragment() {
        return true;
    }

    @Override
    public IBaseFragment getFragmentDetail() {
        return null;
    }
}
